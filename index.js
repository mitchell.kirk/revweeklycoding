function almostPalindrome(word){
    let letter = 0;
    for(let i=0;i<word.length;i++){
        if(word[i] != word[(word.length-1)-i]){
            letter += 1;
        }
    }
    if (letter > 2){
        return false;
    } else return true;
}

function Count(n){
    if(n <=1 ){
        return n;
    }
    return Count(n-1) + Count(n-2);
}
function waysToClimb(c){
    if(c === 0){
        return 1;
    }
    return(Count(c+1))
}

console.log(almostPalindrome("abcdcbg"))
console.log(almostPalindrome("abccia"))
console.log(almostPalindrome("abcdaaa"))
console.log(almostPalindrome("1234312"))

console.log(waysToClimb(1))
console.log(waysToClimb(2))
console.log(waysToClimb(5))